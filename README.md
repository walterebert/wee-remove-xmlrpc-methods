# Remove XML-RPC Methods

WordPress plugin to remove all WordPress XML-RPC methods from the API to increase security.

This plugin does more than just using the [xmlrpc_enabled](https://developer.wordpress.org/reference/hooks/xmlrpc_enabled/) hook, because that is only used “To disable XML-RPC methods that require authentication”.

Activating this plugin will also disable pingbacks, trackbacks, and Really Simple Discovery (RSD), because these rely on XML-RPC.

It works with any webserver, because it does not use the .htaccess file.

## Testing the plugin

From the command line you can test if the plugin is working correctly using [cURL](https://curl.haxx.se/). Replace the `example.com` link to match your website:

```
curl -d '<?xml version="1.0"?><methodCall><methodName>system.listMethods</methodName><params><param><value><string/></value></param></params></methodCall>' https://example.com/xmlrpc.php
```

This should only return the following methods:
- `system.multicall`
- `system.listMethods`
- `system.getCapabilities`

## Author
[Walter Ebert](https://wee.press/)

## License
[GPL-2.0-or-later](https://spdx.org/licenses/GPL-2.0-or-later.html)
